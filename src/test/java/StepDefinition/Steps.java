package StepDefinition;


import BrowserLibrary.CodectorDriverFactory;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Steps {


    // Setup dosyasi okunur
    private String cfgReader(String word) throws IOException {
        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/setup.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        obj.getProperty(word);

        return obj.getProperty(word);
    }

    @After
    public static byte[] takeFailureScreenshot(Scenario scenario) {
        if (scenario.isFailed()) {
            try {
                BufferedImage image = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(CodectorDriverFactory.getTLDriver()).getImage();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(image, "png", baos);

                baos.flush();
                byte[] imageInByte = baos.toByteArray();
                baos.close();
                scenario.embed(imageInByte, "image/png");
                return imageInByte;
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return "Unable to Get Screenshot.".getBytes();
    }

    @When("^Open browser and go to ([^\"]*)$")
    public void open_browser_and_go_to_Url(String word) {
        try {
            CodectorDriverFactory.getTLDriver().manage().window().maximize();
            CodectorDriverFactory.getTLDriver().get(cfgReader(word));
        } catch (Exception e) {
            System.out.println("mesaj:" + e);
        }
    }


    @And("^Click \"([^\"]*)\" element$")
    public void click_setXPath_button(String word) throws Throwable {

        try {
            CodectorDriverFactory.getWait((CodectorDriverFactory.getTLDriver())).until(ExpectedConditions.elementToBeClickable(By.xpath(cfgReader(word))));
            CodectorDriverFactory.getTLDriver().findElement(By.xpath(cfgReader(word))).click();
        } catch (Exception e) {

            throw new Throwable();
        }
    }

}

