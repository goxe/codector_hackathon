package BrowserLibrary;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;


public class TestBase {

    private WebDriverWait wait;

    //Test setup'i set edilir
    @BeforeMethod
    @Parameters(value={"browser"})
    public void setupTest (@Optional String browser){
        //Browser set edilir
        CodectorDriverFactory.setTLDriver(browser);
        wait = new WebDriverWait(CodectorDriverFactory.getTLDriver(), 15);
    }

    @AfterMethod
    public synchronized void tearDown(){
        CodectorDriverFactory.getTLDriver().quit();
    }

}
