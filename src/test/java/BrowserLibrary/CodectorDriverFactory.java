package BrowserLibrary;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;


public class CodectorDriverFactory {

    private static ThreadLocal<WebDriver> tlDriver = new ThreadLocal<>();

    static synchronized void setTLDriver(String browser) {
        if (browser.equals("firefox")) {

            //Dcokerize firefox remote driver kullanimi
            try {
                tlDriver.set(new RemoteWebDriver(new URL("http://3.18.102.200:4444/wd/hub"), BrowserLibrary.OptionsManager.getFirefoxOptions()));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else if (browser.equals("chrome")) {

            //Dcokerize chrome remote driver kullanimi
            try {
                tlDriver.set(new RemoteWebDriver(new URL("http://3.18.102.200:4444/wd/hub"), BrowserLibrary.OptionsManager.getChromeOptions()));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

    }

    //Webdriver bekleme suresi
    //Todo sureyi optimize et (15 saniye)
    public static synchronized WebDriverWait getWait(WebDriver driver) {
        return new WebDriverWait(driver, 3);
    }

    public static synchronized WebDriver getTLDriver() {
        return tlDriver.get();
    }
}